import { ArticleSortFieldEnum } from '@/entities/Article'
import { classNames } from '@/shared/lib/classNames/classNames'
import { SortOrder } from '@/shared/types/sort'
import { ListBox } from '@/shared/ui/Popups'
import { ISelectOption } from '@/shared/ui/Select/Select'
import { VStack } from '@/shared/ui/Stack'
import { Text } from '@/shared/ui/Text/Text'
import { memo, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import cls from './ArticleSortSelector.module.scss'

interface ArticleSortSelectorProps {
    className?: string
    sort: ArticleSortFieldEnum
    order: SortOrder
    onChangeOrder: (newOrder: SortOrder) => void
    onChangeSort: (newSort: ArticleSortFieldEnum) => void
}

export const ArticleSortSelector = memo((props: ArticleSortSelectorProps) => {
    const { className, onChangeOrder, onChangeSort, order, sort } = props
    const { t } = useTranslation()

    const orderOptions = useMemo<ISelectOption<SortOrder>[]>(
        () => [
            {
                value: 'asc',
                content: t('возрастанию'),
            },
            {
                value: 'desc',
                content: t('убыванию'),
            },
        ],
        [t],
    )

    const sortFieldOptions = useMemo<ISelectOption<ArticleSortFieldEnum>[]>(
        () => [
            {
                value: ArticleSortFieldEnum.CREATED,
                content: t('дате создания'),
            },
            {
                value: ArticleSortFieldEnum.TITLE,
                content: t('названию'),
            },
            {
                value: ArticleSortFieldEnum.VIEWS,
                content: t('просмотрам'),
            },
        ],
        [t],
    )

    return (
        <div className={classNames(cls.ArticleSortSelectorRedesigned, [className])}>
            <VStack gap="8">
                <Text text={t('Сортировать по:')} />
                <ListBox items={sortFieldOptions} value={sort} onChange={onChangeSort} />
                <ListBox items={orderOptions} value={order} onChange={onChangeOrder} />
            </VStack>
        </div>
    )
})
