import { ArticleTypeEnum } from '@/entities/Article'
import { classNames } from '@/shared/lib/classNames/classNames'
import { TabItem, Tabs } from '@/shared/ui/Tabs/Tabs'
import { memo, useCallback, useMemo } from 'react'
import { useTranslation } from 'react-i18next'

interface ArticleTypeTabsProps {
    className?: string
    value: ArticleTypeEnum
    onChangeType: (type: ArticleTypeEnum) => void
}

export const ArticleTypeTabs = memo((props: ArticleTypeTabsProps) => {
    const { className, value, onChangeType } = props
    const { t } = useTranslation()

    const typeTabs = useMemo<TabItem[]>(
        () => [
            {
                value: ArticleTypeEnum.ALL,
                content: t('Все статьи'),
            },
            {
                value: ArticleTypeEnum.IT,
                content: t('Айти'),
            },
            {
                value: ArticleTypeEnum.ECONOMICS,
                content: t('Экономика'),
            },
            {
                value: ArticleTypeEnum.SCIENCE,
                content: t('Наука'),
            },
        ],
        [t],
    )

    const onTabClick = useCallback(
        (tab: TabItem) => {
            onChangeType(tab.value as ArticleTypeEnum)
        },
        [onChangeType],
    )

    return (
        <Tabs
            direction="column"
            tabs={typeTabs}
            value={value}
            onTabClick={onTabClick}
            className={classNames('', [className])}
        />
    )
})
