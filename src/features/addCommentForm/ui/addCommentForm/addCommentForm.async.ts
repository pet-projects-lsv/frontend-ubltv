import { FC, lazy } from 'react';
import { IAddCommentFormProps } from './addCommentForm';

export const AddCommentFormAsync = lazy<FC<IAddCommentFormProps>>(
    () =>
        new Promise((resolve) => {
            // @ts-ignore
            // ТАК В РЕАЛЬНЫХ ПРОЕКТАХ НЕ ДЕЛАТЬ!!!!! ДЕЛАЕМ ДЛЯ КУРСА!
            setTimeout(() => resolve(import('./addCommentForm')), 1500);
        })
);
