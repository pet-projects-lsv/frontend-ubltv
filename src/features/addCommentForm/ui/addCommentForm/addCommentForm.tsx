import { classNames } from '@/shared/lib/classNames/classNames'
import { DynamicModuleLoader, IReducersList } from '@/shared/lib/components/DynamicModuleLoader'
import { useAppDispatch } from '@/shared/lib/hooks/useAppDispatch'
import { Button } from '@/shared/ui/Button/Button'
import { Input } from '@/shared/ui/Input/Input'
import { HStack } from '@/shared/ui/Stack'
import { memo, useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import { getAddCommentFormText } from '../../model/selectors/getAddCommentFormSelectors'
import { addCommentFormActions, addCommentFormReducer } from '../../model/slice/addCommentFormSlice'
import cls from './addCommentForm.module.scss'

export interface IAddCommentFormProps {
    className?: string
    onSendComment: (text: string) => void
}

const reducers: IReducersList = {
    addCommentForm: addCommentFormReducer,
}

const AddCommentForm = memo(({ className, onSendComment }: IAddCommentFormProps) => {
    const { t } = useTranslation()
    const dispatch = useAppDispatch()
    const text = useSelector(getAddCommentFormText)

    const onCommentTextChange = useCallback(
        (text: string) => {
            dispatch(addCommentFormActions.setText(text))
        },
        [dispatch],
    )

    const onSendHandler = useCallback(() => {
        onSendComment(text || '')
        onCommentTextChange('')
    }, [text, onSendComment, onCommentTextChange])

    return (
        <DynamicModuleLoader reducers={reducers}>
            <HStack
                data-testid="AddCommentForm"
                max
                justify="between"
                className={classNames(cls.addCommentForm, [className])}
            >
                <Input
                    data-testid="AddCommentForm.Input"
                    className={classNames(cls.input)}
                    placeholder={t('Введите текст комментария')}
                    onChange={onCommentTextChange}
                    value={text}
                />
                <Button data-testid="AddCommentForm.Button" variant="clear" onClick={onSendHandler}>
                    {t('Отправить')}
                </Button>
            </HStack>
        </DynamicModuleLoader>
    )
})

export default AddCommentForm
