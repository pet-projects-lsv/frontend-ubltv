import { IStateSchema } from '@/app/providers/StoreProvider';
import { getLoginUsername } from './getLoginUsername';

describe('getLoginUsername.test', () => {
    test('should return name', () => {
        const state: DeepPartial<IStateSchema> = {
            loginForm: {
                username: 'Ivan'
            }
        };
        expect(getLoginUsername(state as IStateSchema)).toEqual('Ivan');
    });

    test('should work with empty state', () => {
        const state: DeepPartial<IStateSchema> = {};
        expect(getLoginUsername(state as IStateSchema)).toEqual('');
    });
});
