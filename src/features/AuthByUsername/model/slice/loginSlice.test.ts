import { loginActions, loginReducer } from '../slice/loginSlice';
import { ILoginSchema } from '../types/LoginSchema';

describe('loginSlice.test', () => {
    test('test set username', () => {
        const state: DeepPartial<ILoginSchema> = { username: 'admin' };
        expect(
            loginReducer(
                state as ILoginSchema,
                loginActions.setUsername('admin')
            )
        ).toEqual({ username: 'admin' });
    });

    test('test set password', () => {
        const state: DeepPartial<ILoginSchema> = { password: '123' };
        expect(
            loginReducer(
                state as ILoginSchema,
                loginActions.setPassword('123123')
            )
        ).toEqual({ password: '123123' });
    });
});
