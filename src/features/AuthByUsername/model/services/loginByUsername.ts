import { IThunkConfig } from '@/app/providers/StoreProvider'
import { IUser, userActions } from '@/entities/User'
import { createAsyncThunk } from '@reduxjs/toolkit'

interface LoginByUsernameProps {
    username: string
    password: string
}

export const loginByUsername = createAsyncThunk<IUser, LoginByUsernameProps, IThunkConfig<string>>(
    'login/loginByUsername',
    async (authData, thunkApi) => {
        const { extra, dispatch, rejectWithValue } = thunkApi

        try {
            // eslint-disable-next-line no-console
            const response = await extra.api.post<IUser>('/login', authData)

            if (!response.data) {
                // eslint-disable-next-line no-console
                throw new Error()
            }

            dispatch(userActions.setAuthData(response.data))
            return response.data
        } catch (e) {
            // eslint-disable-next-line no-console
            console.log(e)
            return rejectWithValue('error')
        }
    },
)
