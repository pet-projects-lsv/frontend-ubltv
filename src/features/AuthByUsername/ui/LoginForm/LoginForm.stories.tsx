import { ComponentStory, ComponentMeta } from '@storybook/react';
import { StoreDecorator } from '@/shared/config/storybook/StoreDecorator/StoreDecorator';
import LoginForm from './LoginForm';

export default {
    title: 'feature/Loginform',
    component: LoginForm,
    argTypes: {
        backgroundColor: { control: 'color' }
    }
} as ComponentMeta<typeof LoginForm>;

const Template: ComponentStory<typeof LoginForm> = (args) => (
    <LoginForm {...args} />
);

export const LoginFormUI = Template.bind({});
LoginFormUI.args = {};
LoginFormUI.decorators = [
    StoreDecorator({
        loginForm: {
            username: 'admin',
            password: '123',
            isLoading: false
        }
    })
];
