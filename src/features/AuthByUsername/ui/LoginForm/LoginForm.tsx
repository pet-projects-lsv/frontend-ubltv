import { classNames } from '@/shared/lib/classNames/classNames'
import { DynamicModuleLoader, IReducersList } from '@/shared/lib/components/DynamicModuleLoader'
import { useAppDispatch } from '@/shared/lib/hooks/useAppDispatch'
import { Button } from '@/shared/ui/Button/Button'
import { Input } from '@/shared/ui/Input/Input'
import { Text } from '@/shared/ui/Text/Text'
import { memo, useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import {
    getLoginError,
    getLoginIsLoading,
    getLoginPassword,
    getLoginUsername,
} from '../../model/selectors/index'
import { loginByUsername } from '../../model/services/loginByUsername'
import { loginActions, loginReducer } from '../../model/slice/loginSlice'
import cls from './LoginForm.module.scss'

export interface ILoginFormProps {
    className?: string
    onSuccess?: () => void
}

const initialReducers: IReducersList = {
    loginForm: loginReducer,
}

const LoginForm = memo(({ className, onSuccess }: ILoginFormProps) => {
    const { t } = useTranslation()
    const dispatch = useAppDispatch()

    const username = useSelector(getLoginUsername)
    const password = useSelector(getLoginPassword)
    const isLoading = useSelector(getLoginIsLoading)
    const error = useSelector(getLoginError)

    const onChangeUsername = useCallback(
        (value: string) => {
            dispatch(loginActions.setUsername(value))
        },
        [dispatch],
    )

    const onChangePassword = useCallback(
        (value: string) => {
            dispatch(loginActions.setPassword(value))
        },
        [dispatch],
    )

    const onLoginClick = useCallback(async () => {
        const result = await dispatch(
            loginByUsername({
                username,
                password,
            }),
        )

        if (result.payload === 'fulfilled') {
            onSuccess?.()
        }
    }, [onSuccess, dispatch, username, password])

    return (
        <DynamicModuleLoader reducers={initialReducers}>
            <div className={classNames(cls.LoginForm, [className])}>
                <Text text={t('Форма авторизации')} />
                {error && <Text text={`${t('ошибка')} ${error}`} variant="error" />}
                <Input
                    value={username}
                    onChange={onChangeUsername}
                    type="text"
                    className={classNames(cls.input)}
                    placeholder={t('Введите имя')}
                    autoFocus
                />
                <Input
                    value={password ?? ''}
                    onChange={onChangePassword}
                    type="text"
                    className={classNames(cls.input)}
                    placeholder={t('Введите пароль')}
                />
                <Button
                    variant="clear"
                    type="button"
                    className={classNames(cls.loginBtn)}
                    onClick={onLoginClick}
                    disabled={isLoading}
                >
                    {t('Войти')}
                </Button>
            </div>
        </DynamicModuleLoader>
    )
})

export default LoginForm
