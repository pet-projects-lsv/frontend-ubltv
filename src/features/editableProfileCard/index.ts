export type { IProfileSchema } from './model/types/editableProfileCardSchema';
export { EditableProfileCard } from './ui/EditableProfileCard/EditableProfileCard';
