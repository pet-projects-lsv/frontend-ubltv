import { CountryEnum } from '@/entities/Country';
import { CurrencyEnum } from '@/entities/Currency';
import { ValidateProfileErrorEnum } from '../../const/const';
import { validateProfileData } from './validateProfileData';

const data = {
    username: 'admin',
    age: 22,
    country: CountryEnum.Armenia,
    lastname: 'ulbi tv',
    first: 'asd',
    city: 'asf',
    currency: CurrencyEnum.USD
};

describe('validateProfileData.test', () => {
    test('success', async () => {
        const result = validateProfileData(data);

        expect(result).toEqual([]);
    });

    test('without first and last name', async () => {
        const result = validateProfileData({
            ...data,
            firstname: '',
            lastname: ''
        });

        expect(result).toEqual([ValidateProfileErrorEnum.INCORRECT_USER_DATA]);
    });

    test('incorrect age', async () => {
        const result = validateProfileData({ ...data, age: undefined });

        expect(result).toEqual([ValidateProfileErrorEnum.INCORRECT_AGE]);
    });

    test('incorrect country', async () => {
        const result = validateProfileData({ ...data, country: undefined });

        expect(result).toEqual([ValidateProfileErrorEnum.INCORRECT_COUNTRY]);
    });

    test('incorrect all', async () => {
        const result = validateProfileData({});

        expect(result).toEqual([
            ValidateProfileErrorEnum.INCORRECT_USER_DATA,
            ValidateProfileErrorEnum.INCORRECT_AGE,
            ValidateProfileErrorEnum.INCORRECT_COUNTRY
        ]);
    });
});
