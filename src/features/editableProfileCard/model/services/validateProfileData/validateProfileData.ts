import { IProfile } from '@/entities/Profile';
import { ValidateProfileErrorEnum } from '../../const/const';

export const validateProfileData = (profile?: IProfile) => {
    if (!profile) {
        return [ValidateProfileErrorEnum.NO_DATA];
    }

    const { firstname, lastname, age, country } = profile;

    const errors: ValidateProfileErrorEnum[] = [];

    if (!firstname || !lastname) {
        errors.push(ValidateProfileErrorEnum.INCORRECT_USER_DATA);
    }

    if (!age || !Number.isInteger(age)) {
        errors.push(ValidateProfileErrorEnum.INCORRECT_AGE);
    }

    if (!country) {
        errors.push(ValidateProfileErrorEnum.INCORRECT_COUNTRY);
    }

    return errors;
};
