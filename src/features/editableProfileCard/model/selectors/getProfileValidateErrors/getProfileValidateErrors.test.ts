import { IStateSchema } from '@/app/providers/StoreProvider';
import { ValidateProfileErrorEnum } from '../../const/const';
import { getProfileValidateErrors } from './getProfileValidateErrors';

describe('getProfileValidateErrors.test', () => {
    test('should work with filled state', () => {
        const state: DeepPartial<IStateSchema> = {
            profile: {
                validateErrors: [
                    ValidateProfileErrorEnum.SERVER_ERROR,
                    ValidateProfileErrorEnum.INCORRECT_AGE
                ]
            }
        };
        expect(getProfileValidateErrors(state as IStateSchema)).toEqual([
            ValidateProfileErrorEnum.SERVER_ERROR,
            ValidateProfileErrorEnum.INCORRECT_AGE
        ]);
    });
    test('should work with empty state', () => {
        const state: DeepPartial<IStateSchema> = {};
        expect(getProfileValidateErrors(state as IStateSchema)).toEqual(
            undefined
        );
    });
});
