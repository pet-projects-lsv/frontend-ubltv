import { IProfile } from '@/entities/Profile/model/types/profile';
import { ValidateProfileErrorEnum } from '../const/const';

export interface IProfileSchema {
    data?: IProfile
    form?: IProfile
    isLoading: boolean
    error?: string
    readonly: boolean
    validateErrors?: ValidateProfileErrorEnum[]
}
