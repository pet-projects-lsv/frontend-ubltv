import { IArticle } from '@/entities/Article';
import { rtkApi } from '@/shared/api/rtkApi';

const recommendationsApi = rtkApi.injectEndpoints({
    endpoints: (build) => ({
        getArticleRecommendationList: build.query<IArticle[], number>({
            query: (limit: number) => ({
                url: '/articles',
                params: {
                    _limit: limit
                }
            })
        }),
        createArticleRecommendation: build.mutation({
            query: (limit: number) => ({
                url: '/articles',
                params: {
                    _limit: limit
                },
                method: 'PUT'
            })
        })
    })
});

export const useArticleRecommendationList =
    recommendationsApi.useGetArticleRecommendationListQuery;

export const useCreateArticleRecommendation =
    recommendationsApi.useCreateArticleRecommendationMutation;
