import { ArticleList } from '@/entities/Article'
import { classNames } from '@/shared/lib/classNames/classNames'
import { VStack } from '@/shared/ui/Stack'
import { Text } from '@/shared/ui/Text/Text'
import { memo } from 'react'
import { useTranslation } from 'react-i18next'
import { useArticleRecommendationList } from '../../api/ArticleRecommendationsApi'

interface ArticleRecommendationListProps {
    className?: string
}

export const ArticleRecommendationList = memo((props: ArticleRecommendationListProps) => {
    const { className } = props
    const { t } = useTranslation()
    const { data: articles, isLoading, isError } = useArticleRecommendationList(3)

    if (isLoading || isError || !articles) {
        return null
    }

    return (
        <VStack
            data-testid="ArticleRecommendationsList"
            gap="8"
            className={classNames('', [className])}
        >
            <Text size="l" title={t('Рекомендуем')} />
            <ArticleList articles={articles} target="_blank" />
        </VStack>
    )
})
