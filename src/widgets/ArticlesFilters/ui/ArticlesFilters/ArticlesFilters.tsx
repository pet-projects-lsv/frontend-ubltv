import { ArticleSortFieldEnum, ArticleTypeEnum } from '@/entities/Article'
import { ArticleSortSelector } from '@/features/ArticleSortSelector'
import { ArticleTypeTabs } from '@/features/ArticleTypeTabs'
import { classNames } from '@/shared/lib/classNames/classNames'
import { SortOrder } from '@/shared/types/sort'
import { Card } from '@/shared/ui/Card/Card'
import { Input } from '@/shared/ui/Input/Input'
import { VStack } from '@/shared/ui/Stack'
import { memo } from 'react'
import { useTranslation } from 'react-i18next'
import cls from './ArticlesFilters.module.scss'

interface ArticlesFiltersProps {
    className?: string
    sort: ArticleSortFieldEnum
    order: SortOrder
    type: ArticleTypeEnum
    search: string
    onChangeSearch: (value: string) => void
    onChangeOrder: (newOrder: SortOrder) => void
    onChangeSort: (newSort: ArticleSortFieldEnum) => void
    onChangeType: (type: ArticleTypeEnum) => void
}

export const ArticlesFilters = memo((props: ArticlesFiltersProps) => {
    const {
        className,
        onChangeType,
        onChangeSearch,
        search,
        onChangeSort,
        sort,
        onChangeOrder,
        order,
        type,
    } = props
    const { t } = useTranslation()

    return (
        <Card className={classNames(cls.ArticlesFilters, [className])} padding="24">
            <VStack gap="32">
                <Input onChange={onChangeSearch} value={search} placeholder={t('Поиск')} />
                <ArticleTypeTabs value={type} onChangeType={onChangeType} className={cls.tabs} />
                <ArticleSortSelector
                    order={order}
                    sort={sort}
                    onChangeOrder={onChangeOrder}
                    onChangeSort={onChangeSort}
                />
            </VStack>
        </Card>
    )
})
