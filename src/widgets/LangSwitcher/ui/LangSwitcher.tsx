import { classNames } from '@/shared/lib/classNames/classNames'
import { Button } from '@/shared/ui/Button/Button'
import { memo } from 'react'
import { useTranslation } from 'react-i18next'
import cls from './LangSwitcher.module.scss'

interface ILangSwitcherProps {
    className?: string
    short?: boolean
}

export const LangSwitcher = memo(({ className, short }: ILangSwitcherProps) => {
    const { t, i18n } = useTranslation()

    const toggle = async () => {
        await i18n.changeLanguage(i18n.language === 'ru' ? 'en' : 'ru')
    }

    return (
        <Button
            onClick={toggle}
            // eslint-disable-next-line i18next/no-literal-string
            variant="clear"
            className={classNames(cls.LangSwitcher, [className])}
        >
            {short ? t('Короткий язык') : t('Язык')}
        </Button>
    )
})
