import { useTheme } from '@/app/providers/ThemeProvider'
import { saveJsonSettings } from '@/entities/User/model/services/saveJsonSettings'
import ThemeIcon from '@/shared/assets/icons/theme.svg'
import { useAppDispatch } from '@/shared/lib/hooks/useAppDispatch'
import { Icon } from '@/shared/ui/Icon/Icon'
import { memo, useCallback } from 'react'

interface IThemeSwitcherProps {
    className?: string
}

export const ThemeSwitcher = memo(({ className }: IThemeSwitcherProps) => {
    const { theme, toggleTheme } = useTheme()
    const dispatch = useAppDispatch()

    const onToggleHandler = useCallback(() => {
        toggleTheme((newTheme) => {
            dispatch(saveJsonSettings({ theme: newTheme }))
        })
    }, [dispatch, toggleTheme])

    return <Icon Svg={ThemeIcon} clickable onClick={onToggleHandler} />
})
