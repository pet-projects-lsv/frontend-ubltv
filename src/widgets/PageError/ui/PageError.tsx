import { classNames } from '@/shared/lib/classNames/classNames'
import { Button } from '@/shared/ui/Button/Button'
import { useTranslation } from 'react-i18next'

interface IPageErrorProps {
    className?: string
}

export const PageError = ({ className }: IPageErrorProps) => {
    const { t } = useTranslation()

    const reloadPage = () => {
        // eslint-disable-next-line no-restricted-globals
        location.reload()
    }

    return (
        <div className={classNames('', [className])}>
            <p>{t('Произошла непредвиденная ошибка')}</p>
            <Button onClick={reloadPage} className="" variant="clear">
                {t('Обновить страницу')}
            </Button>
        </div>
    )
}
