import { type ReactNode, type FC } from 'react';
import './Loader.scss';

interface ILoaderProps {
  className?: string
  children?: ReactNode
}

export const Loader: FC<ILoaderProps> = () => (
    <div className="loader">
        <div className="lds-ripple ">
            <div />
            <div />
        </div>
    </div>
);
