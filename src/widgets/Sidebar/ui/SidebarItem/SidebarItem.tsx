/* eslint-disable i18next/no-literal-string */
import { getUserAuthData } from '@/entities/User'
import { classNames } from '@/shared/lib/classNames/classNames'
import { AppLink } from '@/shared/ui/AppLink/AppLink'
import { memo } from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import { ISidebarItem } from '../../model/types/sidebarTypes'
import cls from './SidebarItem.module.scss'
import { Icon } from '@/shared/ui/Icon'

interface ISidebarItemProps {
    collapsed?: boolean
    item: ISidebarItem
}

export const SidebarItem = memo(({ item, collapsed }: ISidebarItemProps) => {
    const { t } = useTranslation()

    const isAuth = useSelector(getUserAuthData)

    if (item.authOnly && !isAuth) {
        return null
    }

    return (
        <AppLink
            to={item.path}
            className={classNames(cls.itemRedesigned, [], {
                [cls.collapsedRedesigned]: collapsed,
            })}
            activeClassName={cls.active}
        >
            <Icon Svg={item.Icon} />
            <span className={cls.link}>{t(item.text)}</span>
        </AppLink>
    )
})
