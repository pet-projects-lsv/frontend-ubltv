import AboutIcon from '@/shared/assets/icons/link-icon-about.svg';
import MainIcon from '@/shared/assets/icons/link-icon-main.svg';
import ProfileIcon from '@/shared/assets/icons/link-icon-profile.svg';
import { getRouteAbout, getRouteMain, getRouteProfile } from '@/shared/const/router';
import { SVGProps, VFC } from 'react';

export interface ISidebarItem {
    path: string
    text: string
    Icon: VFC<SVGProps<SVGSVGElement>>
    authOnly?: boolean
}

export const SidebarItemsList: ISidebarItem[] = [
    {
        path: getRouteMain(),
        Icon: MainIcon,
        text: 'Главная страница',
        authOnly: false,
    },
    {
        path: getRouteAbout(),
        Icon: AboutIcon,
        text: 'О Нас',
        authOnly: false,
    },
    {
        path: getRouteProfile(''),
        Icon: ProfileIcon,
        text: 'Профиль',
        authOnly: true,
    },
];
