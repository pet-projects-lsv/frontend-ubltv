import { getUserAuthData } from '@/entities/User';
// import AboutIcon from '@/shared/assets/icons/link-icon-about.svg';
// import ArticleIcon from '@/shared/assets/icons/link-icon-articles.svg';
// import MainIcon from '@/shared/assets/icons/link-icon-main.svg';
// import ProfileIcon from '@/shared/assets/icons/link-icon-profile.svg';
import { getRouteAbout, getRouteArticles, getRouteMain, getRouteProfile } from '@/shared/const/router';
import { createSelector } from '@reduxjs/toolkit';
import { ISidebarItem } from '../types/sidebarTypes';

import MainIcon from '@/shared/assets/icons/home.svg';
import ArticleIcon from '@/shared/assets/icons/article.svg';
import AboutIcon from '@/shared/assets/icons/Info.svg';
import ProfileIcon from '@/shared/assets/icons/avatar.svg';

export const getSideBarItems = createSelector(getUserAuthData, (userData) => {
    const sidebarItemsList: ISidebarItem[] = [
        {
            path: getRouteMain(),
            Icon: MainIcon,
            text: 'Главная',
            authOnly: false,
        },
        {
            path: getRouteAbout(),
            Icon: AboutIcon,
            text: 'О Нас',
            authOnly: false,
        },
    ];

    if (userData) {
        sidebarItemsList.push(
            {
                path: getRouteProfile(userData.id),
                Icon: ProfileIcon,
                text: 'Профиль',
                authOnly: true,
            },
            {
                path: getRouteArticles(),
                Icon: ArticleIcon,
                text: 'Статьи',
                authOnly: true,
            }
        );
    }

    return sidebarItemsList;
});
