import { ArticleSortFieldEnum, ArticleViewEnum } from '@/entities/Article';
import { IArticle } from '@/entities/Article/model/types/article';
import { SortOrder } from '@/shared/types/sort';
import { EntityState } from '@reduxjs/toolkit';
import { ArticleTypeEnum } from './article';

export interface IArticlesPageSchema extends EntityState<IArticle> {
    isLoading?: boolean
    error?: string

    // pagination
    page: number
    limit: number
    hasMore: boolean

    // filter
    view: ArticleViewEnum
    order: SortOrder
    sort: ArticleSortFieldEnum
    search: string
    type: ArticleTypeEnum

    _inited: boolean
}
