import { ArticlePageGreeting } from '@/features/articlePageGreeting'
import { StickyContentLayout } from '@/shared/layouts/StickyContentLayout'
import { classNames } from '@/shared/lib/classNames/classNames'
import { DynamicModuleLoader, IReducersList } from '@/shared/lib/components/DynamicModuleLoader'
import { useAppDispatch } from '@/shared/lib/hooks/useAppDispatch'
import { useInitialEffect } from '@/shared/lib/hooks/useInitialEffect/useInitialEffect'
import { Page } from '@/widgets/Page/Page'
import { memo, useCallback } from 'react'
import { useSearchParams } from 'react-router-dom'
import { fetchNextArticlesPage } from '../../model/services/fetchNextArticlesPage/fetchNextArticlesPage'
import { initArticlesPage } from '../../model/services/initArticlePage/initArticlePage'
import { articlesPageReducer } from '../../model/slices/articlesPageSlice'
import { ArticleInfiniteList } from '../ArticleInfiniteList/ArticleInfiniteList'
import { ViewSelectorContainer } from '../ViewSelectorContainer/ViewSelectorContainer'
import cls from './ArticlesPage.module.scss'
import { FiltersContainer } from '../FiltersContainer/FiltersContainer'

interface IArticlesPageProps {
    className?: string
}

const reducers: IReducersList = {
    articlesPage: articlesPageReducer,
}

export const ArticlesPage = ({ className }: IArticlesPageProps) => {
    const dispatch = useAppDispatch()
    const [searchParams] = useSearchParams()

    const onLoadNextPart = useCallback(() => {
        dispatch(fetchNextArticlesPage())
    }, [dispatch])

    useInitialEffect(() => {
        dispatch(initArticlesPage(searchParams))
    })

    const content = (
        <StickyContentLayout
            left={<ViewSelectorContainer />}
            right={<FiltersContainer />}
            content={
                <Page
                    data-testid="ArticlesPage"
                    onScrollEnd={onLoadNextPart}
                    className={classNames(cls.ArticlesPageRedesigned, [className])}
                >
                    <ArticleInfiniteList className={cls.list} />
                    <ArticlePageGreeting />
                </Page>
            }
        />
    )

    return (
        <DynamicModuleLoader reducers={reducers}>
            {content}
            {/* <Page
                data-testid="ArticlesPage"
                onScrollEnd={onLoadNextPart}
                className={classNames(cls.ArticlesPage, [className])}
            >
                <ArticlesPageFilters />
                <ArticleInfiniteList className={classNames(cls.list)} />
                <ArticlePageGreeting />
            </Page> */}
        </DynamicModuleLoader>
    )
}

export default memo(ArticlesPage)
