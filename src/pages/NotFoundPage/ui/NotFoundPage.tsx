import { useTranslation } from 'react-i18next';

const NotFoundPage = () => {
    const { t } = useTranslation();
    return (
        <div data-testid="NotFoundPage">
            {t('Страница не найдена')}
        </div>
    );
};

export default NotFoundPage;
