import { HStack } from '@/shared/ui/Stack';
import { Page } from '@/widgets/Page/Page';
import { useTranslation } from 'react-i18next';

const AdminPanelPage = () => {
    const { t } = useTranslation('main');
    return (
        <Page data-testid="AdminPanel">
            <HStack>{t('Админка')}</HStack>
        </Page>
    );
};

export default AdminPanelPage;
