import { AboutPage } from './AboutPage/index';
import { ArticlesDetailsPage } from './ArticlesDetailsPage';
import { ArticlesPage } from './ArticlesPage';
import { MainPage } from './MainPage/index';
import { NotFoundPage } from './NotFoundPage/index';
import { ProfilePage } from './ProfilePage/index';
import { TeamPage } from './TeamPage/index';

export {
    AboutPage,
    ArticlesDetailsPage,
    ArticlesPage,
    MainPage,
    NotFoundPage,
    ProfilePage,
    TeamPage,
};
