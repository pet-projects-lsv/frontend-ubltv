export type { IArticleDetailsPageSchema } from './model/types';
export type { IArticleDetailsCommentsSchema } from './model/types/ArticleDetailsCommentsSchema';
export type { IArticleDetailsRecommendationsSchema } from './model/types/ArticleDetailsRecommendationsSchema';
export { ArticlesDetailsPageAsync as ArticlesDetailsPage } from './ui/ArticlesDetailsPage/ArticlesDetailsPage.async';
