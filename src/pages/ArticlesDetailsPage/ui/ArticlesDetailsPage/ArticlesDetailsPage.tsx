/* eslint-disable max-len */
import { ArticleDetails } from '@/entities/Article'
import { ArticleRecommendationList } from '@/features/ArticleRecommendationList'
import ArticleRating from '@/features/articleRating/ui/ArticleRating/ArticleRating'
import { classNames } from '@/shared/lib/classNames/classNames'
import { DynamicModuleLoader, IReducersList } from '@/shared/lib/components/DynamicModuleLoader'
import { ToggleFeatures } from '@/shared/lib/features'
import { Card } from '@/shared/ui/Card/Card'
import { VStack } from '@/shared/ui/Stack'
import { Page } from '@/widgets/Page/Page'
import { memo } from 'react'
import { useParams } from 'react-router-dom'
import { articleDetailsPageReducer } from '../../model/slices'
import { ArticleDetailsComments } from '../ArticleDetailsComments/ArticleDetailsComments'
import { ArticleDetailsPageHeader } from '../ArticleDetailsPageHeader/ArticleDetailsPageHeader'
import cls from './ArticlesDetailsPage.module.scss'
import { useTranslation } from 'react-i18next'

interface IArticlesDetailsPageProps {
    className?: string
}

const reducers: IReducersList = {
    articleDetailsPage: articleDetailsPageReducer,
}

const ArticlesDetailsPage = ({ className }: IArticlesDetailsPageProps) => {
    const { id } = useParams<{ id: string }>()
    const { t } = useTranslation('article-details')

    if (!id) {
        return null
    }

    return (
        <DynamicModuleLoader reducers={reducers}>
            <Page className={classNames(cls.ArticlesDetailsPage, [className])}>
                <VStack max gap="16">
                    <ArticleDetailsPageHeader />
                    <ArticleDetails id={id} />
                    <ArticleRating articleId={id} />
                    <ArticleRecommendationList />
                    <ArticleDetailsComments id={id} />
                    <ToggleFeatures
                        feature="isArticleRatingEnabled"
                        on={<ArticleRating articleId={id} />}
                        off={<Card>{t('Оценка статей скоро появится!')}</Card>}
                    />
                </VStack>
            </Page>
        </DynamicModuleLoader>
    )
}

export default memo(ArticlesDetailsPage)
