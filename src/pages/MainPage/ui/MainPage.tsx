import { HStack } from '@/shared/ui/Stack';
import { Page } from '@/widgets/Page/Page';
import { useTranslation } from 'react-i18next';

const MainPage = () => {
    const { t } = useTranslation('main');

    return (
        <Page data-testid="MainPage">
            <HStack>{t('Главная страница')}</HStack>
        </Page>
    );
};

export default MainPage;
