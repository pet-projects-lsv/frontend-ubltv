import { lazy } from 'react';

export const TeamPageAsync = lazy(async () => await new Promise((res) => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    setTimeout(() => { res(import('./TeamPage')); }, 1000);
}));
