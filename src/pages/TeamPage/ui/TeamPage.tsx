import { useTranslation } from 'react-i18next';
import { Page } from '@/widgets/Page/Page';

const TeamPage = () => {
    const { t } = useTranslation();
    return <Page>{t('Команда')}</Page>;
};

export default TeamPage;
