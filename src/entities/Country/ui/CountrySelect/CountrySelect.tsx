import { memo, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { ListBox } from '@/shared/ui/Popups';
import { CountryEnum } from '../../model/types/country';

interface ICountrySelectProps {
    className?: string
    value?: CountryEnum
    readonly?: boolean
    onChange?: (value: CountryEnum) => void
}

const options = [
    { value: CountryEnum.Armenia, content: 'Armenia' },
    { value: CountryEnum.Belarus, content: 'Belarus' },
    { value: CountryEnum.Kazakhstan, content: 'Kazakhstan' },
    { value: CountryEnum.Russia, content: 'Russia' }
];

export const CountrySelect = memo(
    ({ className, value, onChange, readonly = false }: ICountrySelectProps) => {
        const { t } = useTranslation();

        const onChangeHandler = useCallback(
            (value: string) => {
                onChange?.(value as CountryEnum);
            },
            [onChange]
        );

        return (
            <ListBox
                className={className}
                items={options}
                label={t('Укажите страну')}
                value={value}
                onChange={onChangeHandler}
                readonly={readonly}
                direction="top right"
            />
        );
    }
);
