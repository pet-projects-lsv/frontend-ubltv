import { memo, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { ListBox } from '@/shared/ui/Popups';
import { CurrencyEnum } from '../../model/types/currency';

interface ICurrencySelectProps {
    className?: string
    value?: CurrencyEnum
    readonly?: boolean
    onChange?: (value: CurrencyEnum) => void
}

const options = [
    { value: CurrencyEnum.RUB, content: 'RUB' },
    { value: CurrencyEnum.USD, content: 'USD' },
    { value: CurrencyEnum.EUR, content: 'EUR' }
];

export const CurrencySelect = memo(
    ({
        className,
        value,
        onChange,
        readonly = false
    }: ICurrencySelectProps) => {
        const { t } = useTranslation();

        const onChangeHandler = useCallback(
            (value: string) => {
                onChange?.(value as CurrencyEnum);
            },
            [onChange]
        );

        return (
            <ListBox
                defaultValue={t('Укажите валюту')}
                className={className}
                items={options}
                label={t('Укажите валюту')}
                value={value}
                onChange={onChangeHandler}
                readonly={readonly}
                direction="top right"
            />
        );
    }
);
