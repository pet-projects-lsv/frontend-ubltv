import { createAsyncThunk } from '@reduxjs/toolkit';
import { IThunkConfig } from '@/app/providers/StoreProvider';
import { IArticle } from '../types/article';

export interface ILoginByUsernameProps {
    username: string
    password: string
}

export const fetchArticleById = createAsyncThunk<
    IArticle,
    string | undefined,
    IThunkConfig<string>
>('articleDetails/fetchArticleById', async (id, { extra, rejectWithValue }) => {
    try {
        if (!id) {
            throw new Error('Нет articleId');
        }
        // @ts-ignore
        const response = await extra.api.get<IProfile>(`/articles/${id}`, {
            params: {
                _expanded: 'user'
            }
        });

        if (!response.data) {
            throw new Error();
        }

        return response.data;
    } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e);
        return rejectWithValue('Вы ввели неправильный логин или пароль');
    }
});
