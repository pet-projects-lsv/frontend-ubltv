import { IUser } from '@/entities/User';
import { ArticleBlockTypeEnum, ArticleTypeEnum } from '../const/const';

export interface EnumArticleBlockBase {
    id: string
    type: ArticleBlockTypeEnum
}

export interface IArticleCodeBlock extends EnumArticleBlockBase {
    type: ArticleBlockTypeEnum.CODE
    code: string
}

export interface IArticleImageBlock extends EnumArticleBlockBase {
    type: ArticleBlockTypeEnum.IMAGE
    src: string
    title: string
}

export interface IArticleTextBlock extends EnumArticleBlockBase {
    type: ArticleBlockTypeEnum.TEXT
    paragraphs: string[]
    title?: string
}

export type IArticleBlock =
    | IArticleCodeBlock
    | IArticleImageBlock
    | IArticleTextBlock

export interface IArticle {
    id: string
    title: string
    user: IUser
    subtitle: string
    img: string
    views: number
    createdAt: string
    type: ArticleTypeEnum[]
    blocks: IArticleBlock[]
}
