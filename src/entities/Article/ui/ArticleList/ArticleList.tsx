import { classNames } from '@/shared/lib/classNames/classNames'
import { Text } from '@/shared/ui/Text/Text'
import { HTMLAttributeAnchorTarget, memo } from 'react'
import { useTranslation } from 'react-i18next'
import { ArticleViewEnum } from '../../model/const/const'
import { IArticle } from '../../model/types/article'
import { ArticleListItem } from '../ArticleListItem/ArticleListItem'
import { ArticleListItemSkeleton } from '../ArticleListItem/ArticleListItemSkeleton'
import cls from './ArticleList.module.scss'

interface ArticleListProps {
    className?: string
    articles: IArticle[]
    isLoading?: boolean
    target?: HTMLAttributeAnchorTarget
    view?: ArticleViewEnum
}

const getSkeletons = (view: ArticleViewEnum) =>
    new Array(view === ArticleViewEnum.SMALL ? 9 : 3)
        .fill(0)
        .map((item, index) => (
            <ArticleListItemSkeleton className={cls.card} key={index} view={view} />
        ))

export const ArticleList = memo((props: ArticleListProps) => {
    const { className, articles, view = ArticleViewEnum.SMALL, isLoading, target } = props
    const { t } = useTranslation()

    if (!isLoading && !articles.length) {
        return (
            <div className={classNames(cls.ArticleList, [className, cls[view]])}>
                <Text size="l" title={t('Статьи не найдены')} />
            </div>
        )
    }

    return (
        <div
            className={classNames(cls.ArticleList, [className, cls[view]])}
            data-testid="ArticleList"
        >
            {articles.map((item) => (
                <ArticleListItem
                    article={item}
                    view={view}
                    target={target}
                    key={item.id}
                    className={cls.card}
                />
            ))}
            {isLoading && getSkeletons(view)}
        </div>
    )
})
