import CalendarIcon from '@/shared/assets/icons/calendar-20-20.svg'
import EyeIcon from '@/shared/assets/icons/eye-20-20.svg'
import { classNames } from '@/shared/lib/classNames/classNames'
import { DynamicModuleLoader, IReducersList } from '@/shared/lib/components/DynamicModuleLoader'
import { useAppDispatch } from '@/shared/lib/hooks/useAppDispatch'
import { Avatar } from '@/shared/ui/Avatar/Avatar'
import { Icon } from '@/shared/ui/Icon/Icon'
import { Skeleton } from '@/shared/ui/Skeleton/Skeleton'
import { HStack, VStack } from '@/shared/ui/Stack'
import { Text } from '@/shared/ui/Text/Text'
import { memo, useCallback, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import { ArticleBlockTypeEnum } from '../../model/const/const'
import {
    getArticleDetailsData,
    getArticleDetailsError,
    getArticleDetailsIsLoading,
} from '../../model/selectors/getArticleDetails'
import { fetchArticleById } from '../../model/servies/fetchArticleById'
import { articleDetailsReducer } from '../../model/slice/articleDetailsSlice'
import { IArticleBlock } from '../../model/types/article'
import { ArticleCodeBlockComponent } from '../ArticleCodeBlockComponent/ArticleCodeBlockComponent'
import { ArticleImageBlockComponent } from '../ArticleImageBlockComponent/ArticleImageBlockComponent'
import { ArticleTextBlockComponent } from '../ArticleTextBlockComponent/ArticleTextBlockComponent'
import cls from './ArticleDetails.module.scss'

interface IArticleDetailsProps {
    className?: string
    id?: string
}

const reducers: IReducersList = {
    articleDetails: articleDetailsReducer,
}

export const ArticleDetails = memo(({ className, id }: IArticleDetailsProps) => {
    const { t } = useTranslation()
    const dispatch = useAppDispatch()

    const isLoading = useSelector(getArticleDetailsIsLoading)
    const article = useSelector(getArticleDetailsData)
    const error = useSelector(getArticleDetailsError)

    const renderBlock = useCallback((block: IArticleBlock) => {
        switch (block.type) {
            case ArticleBlockTypeEnum.CODE:
                return <ArticleCodeBlockComponent block={block} />
            case ArticleBlockTypeEnum.IMAGE:
                return <ArticleImageBlockComponent block={block} />
            case ArticleBlockTypeEnum.TEXT:
                return <ArticleTextBlockComponent block={block} />
            default:
                return null
        }
    }, [])

    useEffect(() => {
        if (__PROJECT__ !== 'storybook') {
            dispatch(fetchArticleById(id))
        }
    }, [dispatch, id])

    let content

    if (isLoading) {
        content = (
            <div>
                <Skeleton
                    className={classNames(cls.avatar)}
                    width={100}
                    height={100}
                    border="50%"
                />
                <Skeleton className={classNames(cls.title)} width={300} height={32} />
                <Skeleton className={classNames(cls.skeleton)} width={600} height={24} />
                <Skeleton className={classNames(cls.skeleton)} width="100%" height={200} />
                <Skeleton className={classNames(cls.skeleton)} width="100%" height={200} />
            </div>
        )
    } else if (error) {
        content = <Text title={t('Произошла ошибка при загрузке статьи')} align="center" />
    } else {
        content = (
            <>
                <HStack justify="center" max>
                    <Avatar size={100} src={article?.img} className={cls.avatar} />
                </HStack>

                <VStack gap="4" max data-testid="ArticleDetails.Info">
                    <Text title={article?.title} text={article?.subtitle} />

                    <HStack gap="8">
                        <Icon className={cls.icon} Svg={EyeIcon} />
                        <Text text={String(article?.views)} />
                    </HStack>

                    <HStack gap="8">
                        <Icon className={cls.icon} Svg={CalendarIcon} />
                        <Text text={article?.createdAt} />
                    </HStack>
                </VStack>

                {article?.blocks.map(
                    (block: IArticleBlock) => renderBlock(block),
                    // eslint-disable-next-line function-paren-newline
                )}
            </>
        )
    }

    return (
        <DynamicModuleLoader reducers={reducers}>
            <VStack max gap="16" className={classNames(cls.ArticleDetails, [className])}>
                {content}
            </VStack>
        </DynamicModuleLoader>
    )
})
