import { FeatureFlags } from '@/shared/types/featureFlags';
import { UserRoleEnum } from '../const/const';
import { JsonSettings } from './jsonSettings';

export interface IUser {
    id: string
    username: string
    avatar?: string
    roles?: UserRoleEnum[]
    features?: FeatureFlags;
    jsonSettings?: JsonSettings;
}

export interface IUserSchema {
    authData?: IUser
    _inited?: boolean
}
