import { IThunkConfig } from '@/app/providers/StoreProvider'
import { createAsyncThunk } from '@reduxjs/toolkit'
import { JsonSettings } from '../types/jsonSettings'
import { setJsonSettingsMutation } from '../api/userApi'
import { getUserAuthData } from '../selectors/getUserAuthData'
import { getJsonSettings } from '../selectors/jsonSettings'

export const saveJsonSettings = createAsyncThunk<JsonSettings, JsonSettings, IThunkConfig<string>>(
    'user/saveJsonSettings',
    async (newJsonSettings, thunkApi) => {
        const { rejectWithValue, getState, dispatch } = thunkApi
        const userData = getUserAuthData(getState())
        const currentSettings = getJsonSettings(getState())

        if (!userData) {
            return rejectWithValue('')
        }

        try {
            const response = await dispatch(
                setJsonSettingsMutation({
                    userId: userData.id,
                    jsonSettings: {
                        ...currentSettings,
                        ...newJsonSettings,
                    },
                }),
            ).unwrap()

            if (!response.jsonSettings) {
                return rejectWithValue('')
            }

            return response.jsonSettings
        } catch (e) {
            // eslint-disable-next-line no-console
            console.log(e)
            return rejectWithValue('')
        }
    },
)
