import { CountryEnum, CountrySelect } from '@/entities/Country'
import { CurrencyEnum, CurrencySelect } from '@/entities/Currency'
import { IMods, classNames } from '@/shared/lib/classNames/classNames'
import { Avatar } from '@/shared/ui/Avatar/Avatar'
import { Input } from '@/shared/ui/Input/Input'
import { HStack, VStack } from '@/shared/ui/Stack'
import { Text } from '@/shared/ui/Text/Text'
import { Loader } from '@/widgets/Loader'
import { useTranslation } from 'react-i18next'
import { IProfile } from '../index'
import cls from './ProfileCard.module.scss'

interface IProfileCardProps {
    className?: string
    data?: IProfile
    isLoading?: boolean
    error?: string | undefined
    readonly?: boolean
    onChangeLastname?: (lastname: string) => void
    onChangeFirstname?: (firstname: string) => void
    onChangeAge?: (age: string) => void
    onChangeCity?: (city: string) => void
    onChangeUsername?: (username: string) => void
    onChangeAvatar?: (avatar: string) => void
    onChangeCurrency?: (currency: CurrencyEnum) => void
    onChangeCountry?: (country: CountryEnum) => void
}

export const ProfileCard = ({
    className,
    data,
    isLoading,
    error,
    readonly,
    onChangeFirstname,
    onChangeLastname,
    onChangeAge,
    onChangeCity,
    onChangeUsername,
    onChangeAvatar,
    onChangeCurrency,
    onChangeCountry,
}: IProfileCardProps) => {
    const { t } = useTranslation('profile')

    if (error) {
        return (
            <HStack
                gap="8"
                max
                justify="center"
                className={classNames(cls.ProfileCard, [className, cls.isLoading])}
            >
                <Text
                    title={t('Произошла ошибка при зашрузке профиля')}
                    text={t('попробуйте обновить страницу')}
                    variant="error"
                    align="center"
                />
            </HStack>
        )
    }

    if (isLoading) {
        return (
            <HStack
                max
                justify="center"
                className={classNames(cls.ProfileCard, [className, cls.isLoading])}
            >
                <Loader />
            </HStack>
        )
    }

    const mods: IMods = {
        [cls.editing]: !readonly,
    }

    return (
        <VStack gap="16" max className={classNames(cls.ProfileCard, [className], mods)}>
            <HStack max justify="center">
                {data?.avatar && <Avatar alt="123" src={data?.avatar} size={100} />}
            </HStack>

            <Input
                readonly={readonly}
                value={data?.firstname}
                onChange={onChangeFirstname}
                placeholder={t('Ваше имя')}
                data-testid="ProfileCard.firstname"
            />
            <Input
                readonly={readonly}
                value={data?.lastname}
                onChange={onChangeLastname}
                placeholder={t('Ваша фамилия')}
                data-testid="ProfileCard.lastname"
            />
            <Input
                readonly={readonly}
                onChange={onChangeAge}
                value={String(data?.age)}
                data-testid="ProfileCard.age"
                placeholder={t('Ваш возраст')}
            />
            <Input
                value={data?.city}
                readonly={readonly}
                onChange={onChangeCity}
                placeholder={t('Ваш город')}
                data-testid="ProfileCard.city"
            />
            <Input
                readonly={readonly}
                value={data?.username}
                onChange={onChangeUsername}
                placeholder={t('Имя пользователя')}
                data-testid="ProfileCard.username"
            />
            <Input
                readonly={readonly}
                value={data?.avatar}
                onChange={onChangeAvatar}
                placeholder={t('Ваш аватар')}
                data-testid="ProfileCard.avatar"
            />

            <CurrencySelect
                readonly={readonly}
                className={cls.input}
                value={data?.currency}
                onChange={onChangeCurrency}
                data-testid="ProfileCard.currency"
            />

            <CountrySelect
                readonly={readonly}
                className={cls.input}
                value={data?.country}
                onChange={onChangeCountry}
                data-testid="ProfileCard.country"
            />
        </VStack>
    )
}
