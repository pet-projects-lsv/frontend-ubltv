import { ComponentMeta, ComponentStory } from '@storybook/react';
import { CountryEnum } from '@/entities/Country';
import { CurrencyEnum } from '@/entities/Currency';
import avatar from '@/shared/assets/tests/avatar.jpg';
import { ProfileCard } from '../../Profile';

export default {
    title: 'entities/ProfileCard',
    component: ProfileCard,
    argTypes: {
        backgroundColor: { control: 'color' }
    }
} as ComponentMeta<typeof ProfileCard>;

const Template: ComponentStory<typeof ProfileCard> = (args) => (
    <ProfileCard {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
    data: {
        username: 'admin',
        age: 22,
        country: CountryEnum.Belarus,
        lastname: 'ulbi tv',
        firstname: 'asd',
        city: 'asf',
        currency: CurrencyEnum.RUB,
        avatar
    }
};

export const withError = Template.bind({});
withError.args = {
    error: 'true'
};

export const Loading = Template.bind({});
Loading.args = {
    isLoading: true
};
