import { IReduxStoreWithManager } from '@/app/providers/StoreProvider';
import { IStateSchema, IStateSchemaKey } from '@/app/providers/StoreProvider/config/StateSchema';
import { Reducer } from '@reduxjs/toolkit';
import { FC, ReactNode, useEffect } from 'react';
import { useDispatch, useStore } from 'react-redux';

export type IReducersList = {
    [name in IStateSchemaKey]?: Reducer<NonNullable<IStateSchema[name]>>
}

interface IDynamicModuleLoaderProps {
    reducers: IReducersList
    removeAfterUnmount?: boolean
    children: ReactNode
}

export const DynamicModuleLoader: FC<IDynamicModuleLoaderProps> = ({
    children,
    reducers,
    removeAfterUnmount = true,
}) => {
    const dispatch = useDispatch();
    const store = useStore() as IReduxStoreWithManager;

    // eslint-disable-next-line consistent-return
    useEffect(() => {
        const mountedReducers = store.reducerManager.getMountedReducers();

        Object.entries(reducers).forEach(([name, reducer]) => {
            const mounted = mountedReducers[name as IStateSchemaKey];
            // Добавляем новый редюсер только если его нет
            if (!mounted) {
                store.reducerManager.add(name as IStateSchemaKey, reducer);
                dispatch({ type: `@INIT ${name} reducer` });
            }
        });

        return () => {
            if (removeAfterUnmount) {
                Object.entries(reducers).forEach(([name]) => {
                    store.reducerManager.remove(name as IStateSchemaKey);
                    dispatch({ type: `@DESTROY ${name} reducer` });
                });
            }
        };
        // eslint-disable-next-line
    }, [])
    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <>{children}</>;
};
