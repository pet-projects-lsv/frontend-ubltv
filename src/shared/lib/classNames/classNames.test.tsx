import { classNames } from './classNames';

describe('classNames', () => {
    test('with only first param', () => {
        expect(classNames('someClass')).toBe('someClass');
    });

    test('with additional class', () => {
        expect(classNames('someClass', ['class'])).toBe('someClass class');
    });

    test('with mods', () => {
        expect(classNames('someClass', ['class'], { hovered: true })).toBe('someClass class hovered');
    });

    test('with mods', () => {
        expect(classNames('someClass', ['class'], { hovered: true, error: false })).toBe('someClass class hovered');
    });
});
