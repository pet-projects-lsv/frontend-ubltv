import { RouterDecorator } from './RouterDecorator/RouterDecorator';
import { StyleDecorator } from './StyleDecorator/StyleDecorator';
import { SuspenseDecorator } from './SuspenseDecorator/SuspenseDecorator';
import { ThemeDecorator } from './ThemeDecorator/ThemeDecorator';
import { TranslationDecorator } from './TranslationDecorator/TranslationDecorator';

export {
    RouterDecorator,
    StyleDecorator,
    SuspenseDecorator,
    ThemeDecorator,
    TranslationDecorator
};
