import { UserRoleEnum } from '@/entities/User/model/const/const'
import { RouteProps } from 'react-router-dom'

export type AppRouterProps = RouteProps & {
    authOnly?: boolean
    roles?: UserRoleEnum[]
}
