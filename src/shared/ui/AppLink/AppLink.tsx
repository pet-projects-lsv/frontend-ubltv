import { classNames } from '@/shared/lib/classNames/classNames'
import { ReactNode, memo } from 'react'
import { NavLink, type LinkProps } from 'react-router-dom'
import cls from './AppLink.module.scss'

export type AppLinkVariant = 'primary' | 'red'

interface IAppLinkProps extends LinkProps {
    className?: string
    variant?: AppLinkVariant
    children?: ReactNode
    activeClassName?: string
}

export const AppLink = memo((props: IAppLinkProps) => {
    const {
        children,
        className,
        to,
        variant = 'primary',
        activeClassName = '',
        ...otherProps
    } = props
    return (
        <NavLink
            to={to}
            className={({ isActive }) =>
                classNames(cls.AppLink, [className, cls[variant]], { [activeClassName]: isActive })}
            {...otherProps}
        >
            {children}
        </NavLink>
    )
})
