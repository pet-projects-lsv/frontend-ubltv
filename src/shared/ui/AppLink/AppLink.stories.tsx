import { Theme } from '@/app/providers/ThemeProvider'
import { ThemeDecorator } from '@/shared/config/storybook/ThemeDecorator/ThemeDecorator'
import { ComponentMeta, ComponentStory } from '@storybook/react'
import { AppLink } from './AppLink'

export default {
    title: 'ui/AppLink',
    component: AppLink,
    argTypes: {
        backgroundColor: { control: 'color' },
    },
    args: {
        to: '/',
    },
} as ComponentMeta<typeof AppLink>

const Template: ComponentStory<typeof AppLink> = (args) => <AppLink {...args} />

export const PRIMERY = Template.bind({})
PRIMERY.args = {
    children: 'ссылка',
    // theme: AppLinkThemeEnum.PRIMARY,
}

export const PRIMERY_DARK = Template.bind({})
PRIMERY_DARK.args = {
    children: 'ссылка',
    // theme: AppLinkThemeEnum.PRIMARY,
}
PRIMERY_DARK.decorators = [ThemeDecorator(Theme.DARK)]

export const SECONDARY = Template.bind({})
SECONDARY.args = {
    children: 'ссылка',
    // theme: AppLinkThemeEnum.SECONDARY,
}

export const SECONDARY_DARK = Template.bind({})
SECONDARY_DARK.args = {
    children: 'ссылка',
    // theme: AppLinkThemeEnum.SECONDARY,
}
SECONDARY_DARK.decorators = [ThemeDecorator(Theme.DARK)]
