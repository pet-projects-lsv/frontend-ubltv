import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ThemeDecorator } from '@/shared/config/storybook/ThemeDecorator/ThemeDecorator';
import { Theme } from '@/app/providers/ThemeProvider';
import { Modal } from './Modal';

export default {
    title: 'widget/Navbar',
    component: Modal,
    argTypes: {
        backgroundColor: { control: 'color' }
    }
} as ComponentMeta<typeof Modal>;

const Template: ComponentStory<typeof Modal> = (args) => <Modal {...args} />;

export const Light = Template.bind({});
Light.args = {
    children: `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Et sit
    minus, error deserunt veritatis saepe officiis, minima voluptas
    temporibus eum laudantium modi laborum adipisci numquam soluta
    architecto est voluptates corrupti fuga blanditiis, maiores aperiam
    officia! Fugit omnis consequatur nulla. Dolorem, inventore! Cumque
    veniam suscipit, iure earum ab rem sit eos quo ratione. Similique
    dignissimos quia tempora ducimus, reiciendis, molestiae aut omnis
    quam sit delectus provident iusto fugit pariatur culpa ullam quod,
    fuga autem eveniet? Excepturi sint voluptates natus recusandae non
    vitae id ipsa, obcaecati dignissimos tempora sed in consequatur
    eveniet aperiam vero fugiat velit beatae reprehenderit dolorum`
};

export const Dark = Template.bind({});
Dark.args = {
    children: `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Et sit
      minus, error deserunt veritatis saepe officiis, minima voluptas
      temporibus eum laudantium modi laborum adipisci numquam soluta
      architecto est voluptates corrupti fuga blanditiis, maiores aperiam
      officia! Fugit omnis consequatur nulla. Dolorem, inventore! Cumque
      veniam suscipit, iure earum ab rem sit eos quo ratione. Similique
      dignissimos quia tempora ducimus, reiciendis, molestiae aut omnis
      quam sit delectus provident iusto fugit pariatur culpa ullam quod,
      fuga autem eveniet? Excepturi sint voluptates natus recusandae non
      vitae id ipsa, obcaecati dignissimos tempora sed in consequatur
      eveniet aperiam vero fugiat velit beatae reprehenderit dolorum`
};
Dark.decorators = [ThemeDecorator(Theme.DARK)];
