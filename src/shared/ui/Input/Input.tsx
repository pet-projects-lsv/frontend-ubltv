import {
    ChangeEvent,
    InputHTMLAttributes,
    memo,
    SyntheticEvent,
    useEffect,
    useRef,
    useState
} from 'react';
import { classNames } from '@/shared/lib/classNames/classNames';
import cls from './Input.module.scss';

export enum InputThemeEnum {
  CLEAR = 'clear',
  OUTLINE = 'outline',
  BACKGROUND = 'background',
  BACKGROUND_INVERTED = 'backgroundInverted'
}

type HTMLInputProps = Omit<
  InputHTMLAttributes<HTMLInputElement>,
  'value' | 'onChange' | 'readonly'
>

interface IInputProps extends HTMLInputProps {
  className?: string
  value?: string
  onChange?: (value: string) => void
  theme?: InputThemeEnum
  type?: string
  placeholder?: string
  autoFocus?: boolean
  readonly?: boolean
}

export const Input = memo((props: IInputProps) => {
    const {
        className,
        theme = InputThemeEnum.CLEAR,
        value = '',
        onChange,
        placeholder,
        type = 'text',
        autoFocus,
        readonly,
        ...otherProps
    } = props;

    const [isFocused, setIsFocused] = useState(false);
    const [caretPosition, setCaretPosition] = useState(0);
    const ref = useRef<HTMLInputElement>(null);

    const onChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
        onChange?.(e.currentTarget.value);
        setCaretPosition(e.target.value.length);
    };

    const onBlur = () => {
        setIsFocused(false);
    };

    const onFocus = () => {
        setIsFocused(true);
    };

    const onSelect = (e: SyntheticEvent<HTMLDivElement, Event>) => {
        if (e.target instanceof HTMLInputElement) {
            setCaretPosition(e?.target?.selectionStart || 0);
        }
    };

    useEffect(() => {
        if (autoFocus) {
            setIsFocused(true);
            ref.current?.focus();
        }
    }, [autoFocus]);

    const mods = {
        [cls[theme]]: true,
        [cls.readonly]: readonly
    };

    const isCaretVisible = isFocused && !readonly;

    return (
        <div className={classNames(cls.inputWrapper)}>
            {placeholder && (
                <div className={classNames(cls.placeholder)}>{`${placeholder} >`}</div>
            )}
            <div className={classNames(cls.caretWraper)}>
                <input
                    ref={ref}
                    type={type}
                    value={value}
                    onChange={(e) => onChangeHandler(e)}
                    className={classNames(cls.Input, [className], mods)}
                    onFocus={onFocus}
                    onBlur={onBlur}
                    onSelect={onSelect}
                    readOnly={readonly}
                    {...otherProps}
                />
                {isCaretVisible && (
                    <span
                        className={classNames(cls.caret)}
                        style={{ left: `${caretPosition * 5}px` }}
                    />
                )}
            </div>
        </div>
    );
});
