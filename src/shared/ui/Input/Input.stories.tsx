import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ThemeDecorator } from '@/shared/config/storybook/ThemeDecorator/ThemeDecorator';
import { Theme } from '@/app/providers/ThemeProvider';
import { Input, InputThemeEnum } from './Input';

export default {
    title: 'ui/Input',
    component: Input,
    argTypes: {
        backgroundColor: { control: 'color' }
    }
} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = (args) => <Input {...args} />;

export const Clear = Template.bind({});
Clear.args = {
    placeholder: 'Введите текст',
    theme: InputThemeEnum.CLEAR
};

Clear.decorators = [ThemeDecorator(Theme.DARK)];
