import { ChangeEvent, useMemo } from 'react';
import { classNames, IMods } from '@/shared/lib/classNames/classNames';
import cls from './Select.module.scss';

export interface ISelectOption<T extends string> {
    value: T
    content: string
}

interface ISelectProps<T extends string> {
    className?: string
    label?: string
    options: ISelectOption<T>[]
    value?: T
    onChange?: (value: T) => void
    readonly?: boolean
}

export const Select = <T extends string>({
    className,
    label,
    options,
    value,
    onChange,
    readonly
}: ISelectProps<T>) => {
    const mods: IMods = {};

    const onChangeHandler = (e: ChangeEvent<HTMLSelectElement>) => {
        onChange?.(e.target.value as T);
    };

    const optionsList = useMemo(
        () =>
            options?.map((opt) => (
                <option
                    key={opt.value}
                    className={classNames(cls.option)}
                    value={opt.value}
                >
                    {opt.content}
                </option>
            )),
        [options]
    );

    return (
        <div className={classNames(cls.Wrapper, [className], mods)}>
            {label && <div className={classNames(cls.label)}>{label}</div>}

            <select
                onChange={onChangeHandler}
                className={classNames(cls.select)}
                disabled={readonly}
                value={value}
            >
                {optionsList}
            </select>
        </div>
    );
};
