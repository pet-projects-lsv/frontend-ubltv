import { render, screen } from '@testing-library/react'
import { Button } from './Button'

describe('button', () => {
    test('Test render', () => {
        // eslint-disable-next-line i18next/no-literal-string
        render(<Button>Test</Button>)
        expect(screen.getByText('Test')).toBeInTheDocument()
    })

    test('render with theme', () => {
        // eslint-disable-next-line i18next/no-literal-string
        render(<Button variant="clear">Test</Button>)
        expect(screen.getByText('Test')).toHaveClass('clear')
    })
})
