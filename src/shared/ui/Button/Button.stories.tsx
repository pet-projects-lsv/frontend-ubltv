import { Theme } from '@/app/providers/ThemeProvider'
import { ThemeDecorator } from '@/shared/config/storybook/ThemeDecorator/ThemeDecorator'
import { ComponentMeta, ComponentStory } from '@storybook/react'
import { Button } from './Button'

export default {
    title: 'ui/Button',
    component: Button,
    argTypes: {
        backgroundColor: { control: 'color' },
    },
} as ComponentMeta<typeof Button>

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />

export const Clear = Template.bind({})
Clear.args = {
    children: 'пример',
    variant: 'clear',
}
Clear.decorators = [ThemeDecorator(Theme.DARK)]

export const ClearLigth = Template.bind({})
ClearLigth.args = {
    children: 'пример',
    variant: 'clear',
}

export const Outline = Template.bind({})
Outline.args = {
    children: 'пример',
    variant: 'clear',
}
Outline.decorators = [ThemeDecorator(Theme.DARK)]

export const OutlineLigth = Template.bind({})
OutlineLigth.args = {
    children: 'пример',
    variant: 'clear',
}

export const BagroundTheme = Template.bind({})
BagroundTheme.args = {
    children: 'пример',
    variant: 'clear',
}

export const BagroundInvertedTheme = Template.bind({})
BagroundInvertedTheme.args = {
    children: 'пример',
    variant: 'clear',
}

export const SquareSizeL = Template.bind({})
SquareSizeL.args = {
    children: '>',
    variant: 'clear',
    size: 'l',
}

export const SquareSizeM = Template.bind({})
SquareSizeL.args = {
    children: '>',
    variant: 'clear',
    size: 'm',
}

export const SquareSizeXL = Template.bind({})
SquareSizeXL.args = {
    children: '>',
    variant: 'clear',
    size: 'xl',
}

export const Disabled = Template.bind({})
Disabled.args = {
    children: 'disabled',
    variant: 'clear',
    size: 'xl',
    disabled: true,
}
