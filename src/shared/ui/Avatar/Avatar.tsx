import { CSSProperties, useMemo } from 'react'
import UserIcon from '../../assets/icons/user-filled.svg'
import { AppImage } from '../AppImage'

import { IMods, classNames } from '@/shared/lib/classNames/classNames'
import { Icon } from '../Icon/Icon'
import { Skeleton } from '../Skeleton/Skeleton'
import cls from './Avatar.module.scss'

interface AvatarProps {
    className?: string
    src?: string
    size?: number
    alt?: string
    fallbackInverted?: boolean
}

export const Avatar = ({ className, src, size = 100, alt, fallbackInverted }: AvatarProps) => {
    const mods: IMods = {}

    const styles = useMemo<CSSProperties>(
        () => ({
            width: size,
            height: size,
        }),
        [size],
    )

    const fallback = <Skeleton width={size} height={size} border="50%" />
    const errorFallback = <Icon width={size} height={size} Svg={UserIcon} />

    return (
        <AppImage
            fallback={fallback}
            errorFallback={errorFallback}
            src={src}
            alt={alt}
            style={styles}
            className={classNames(cls.Avatar, [className], mods)}
        />
    )
}
