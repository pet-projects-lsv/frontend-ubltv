import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Avatar } from './Avatar';

export default {
    title: 'ui/Avatar',
    component: Avatar,
    argTypes: {
        backgroundColor: { control: 'color' }
    },
    args: {
        to: '/'
    }
} as ComponentMeta<typeof Avatar>;

const Template: ComponentStory<typeof Avatar> = (args) => <Avatar {...args} />;

export const AvatarStory = Template.bind({});
AvatarStory.args = {
    size: 100,
    // eslint-disable-next-line max-len
    src: 'https://sun9-35.userapi.com/impg/yvx2plAHJSAgWcfXkQWuK6lGtp_jQhzylkNrgQ/uneJLsdtA8E.jpg?size=564x564&quality=95&sign=e494c6f0e9cfe82f45ac6147964220f2&type=album'
};
