import { classNames } from '@/shared/lib/classNames/classNames'
import { memo } from 'react'
import cls from './Text.module.scss'

export type TextVariant = 'primary' | 'inverted' | 'error'

export type TextAlign = 'right' | 'left' | 'center'

export type TextSize = 's' | 'm' | 'l' | 'xl'

interface ITextProps {
    'className'?: string
    'title'?: string
    'text'?: string
    'variant'?: TextVariant
    'align'?: TextAlign
    'size'?: TextSize

    'data-testid'?: string
}

type HeaderTagType = 'h1' | 'h2' | 'h3' | 'h4'

const mapSizeToClass: Record<TextSize, string> = {
    s: 'size_s',
    m: 'size_m',
    l: 'size_l',
    xl: 'size_xl',
}

const mapSizeToHeaderTag: Record<TextSize, HeaderTagType> = {
    s: 'h4',
    m: 'h3',
    l: 'h2',
    xl: 'h1',
}

export const Text = memo(
    ({
        className,
        title,
        text,
        variant = 'primary',
        align = 'left',
        size = 'm',
        'data-testid': dataTestId = 'Text',
    }: ITextProps) => {
        const HeaderTag = mapSizeToHeaderTag[size]
        const sizeClass = mapSizeToClass[size]

        const additionalClasses = [className, cls[variant], cls[align], sizeClass]

        return (
            <div className={classNames(cls.Text, additionalClasses, {})}>
                {title && (
                    <HeaderTag data-testid={`${dataTestId}.Header`} className={cls.title}>
                        {title}
                    </HeaderTag>
                )}
                {text && (
                    <p data-testid={`${dataTestId}.Paragraph`} className={cls.text}>
                        {text}
                    </p>
                )}
            </div>
        )
    },
)
