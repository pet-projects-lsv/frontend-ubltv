import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ThemeDecorator } from '@/shared/config/storybook/ThemeDecorator/ThemeDecorator';
import { Theme } from '@/app/providers/ThemeProvider';
import { Text } from './Text';

export default {
    title: 'ui/Text',
    component: Text,
    argTypes: {
        backgroundColor: { control: 'color' }
    }
} as ComponentMeta<typeof Text>;

const Template: ComponentStory<typeof Text> = (args) => <Text {...args} />;

export const TextDark = Template.bind({});
TextDark.args = {
    text: 'w3r'
};
TextDark.decorators = [ThemeDecorator(Theme.DARK)];

export const TitleLigth = Template.bind({});
TextDark.args = {
    text: 'w3r'
};
TextDark.decorators = [ThemeDecorator(Theme.DARK)];
