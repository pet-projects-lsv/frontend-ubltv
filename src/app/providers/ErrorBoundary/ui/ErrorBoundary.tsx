/* eslint-disable no-console */
import React, { type ReactNode, type ErrorInfo, Suspense } from 'react';
import { Loader } from '@/widgets/Loader';
import { PageError } from '@/widgets/PageError';

interface IErrorBoundaryProp {
    children: ReactNode
}
interface IErrorBoundaryState {
    hasError: boolean
}

class ErrorBoundary extends React.Component<
    IErrorBoundaryProp,
    IErrorBoundaryState
> {
    constructor(props: IErrorBoundaryProp) {
        super(props);
        this.state = { hasError: false };
    }

    static getDerivedStateFromError(error: Error) {
        console.log(error);
        // Обновить состояние с тем, чтобы следующий рендер показал запасной UI.
        return { hasError: true };
    }

    componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    // Можно также сохранить информацию об ошибке в соответствующую службу журнала ошибок
        console.log(error, errorInfo);
    }

    render() {
        const { hasError } = this.state;
        const { children } = this.props;

        if (hasError) {
            // Можно отрендерить запасной UI произвольного вида
            return (
                <Suspense fallback={<Loader />}>
                    <PageError />
                </Suspense>
            );
        }

        return children;
    }
}

export default ErrorBoundary;
