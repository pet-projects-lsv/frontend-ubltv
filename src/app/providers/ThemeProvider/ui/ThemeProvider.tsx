import { useJsonSettings } from '@/entities/User'
import { ThemeEnum } from '@/shared/const/theme'
import { FC, ReactNode, useEffect, useMemo, useState } from 'react'
import { ThemeContext } from '../lib/ThemeContext'

interface ThemeProviderProps {
    initialTheme?: ThemeEnum
    children: ReactNode
}

const ThemeProvider: FC<ThemeProviderProps> = (props) => {
    const { theme: defaultTheme } = useJsonSettings()
    const { initialTheme, children } = props
    const [isThemeInited, setThemeInited] = useState(false)
    const [theme, setTheme] = useState<ThemeEnum>(initialTheme || defaultTheme || ThemeEnum.LIGHT)

    useEffect(() => {
        if (!isThemeInited && defaultTheme) {
            setTheme(defaultTheme)
            setThemeInited(true)
        }
    }, [defaultTheme, isThemeInited])

    const defaultProps = useMemo(
        () => ({
            theme,
            setTheme,
        }),
        [theme],
    )

    document.body.className = theme

    return <ThemeContext.Provider value={defaultProps}>{children}</ThemeContext.Provider>
}

export default ThemeProvider
