import { ThemeEnum } from '@/shared/const/theme'
import { useContext } from 'react'
import { ThemeContext } from './ThemeContext'

interface UseThemeResult {
    toggleTheme: (saveActions?: (theme: ThemeEnum) => void) => void
    theme: ThemeEnum
}

export function useTheme(): UseThemeResult {
    const { theme, setTheme } = useContext(ThemeContext)

    const toggleTheme = (saveAction?: (theme: ThemeEnum) => void) => {
        let newTheme: ThemeEnum

        switch (theme) {
            case ThemeEnum.DARK:
                newTheme = ThemeEnum.LIGHT
                break
            case ThemeEnum.LIGHT:
                newTheme = ThemeEnum.ORANGE
                break
            case ThemeEnum.ORANGE:
                newTheme = ThemeEnum.DARK
                break
            default:
                newTheme = ThemeEnum.LIGHT
                break
        }
        setTheme(newTheme)
        document.body.className = newTheme
        saveAction?.(newTheme)
    }

    return {
        theme: theme || ThemeEnum.LIGHT,
        toggleTheme,
    }
}
