import { ThemeEnum } from '@/shared/const/theme'
import { createContext } from 'react'

export interface ThemeContextProps {
    theme: ThemeEnum
    setTheme: (theme: ThemeEnum) => void
}

// eslint-disable-next-line @typescript-eslint/consistent-type-assertions
export const ThemeContext = createContext<ThemeContextProps>({} as ThemeContextProps)

export const LOCAL_STORAGE_THEME_KEY = 'theme'
