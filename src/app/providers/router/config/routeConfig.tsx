import { UserRoleEnum } from '@/entities/User/model/const/const'
import {
    AboutPage,
    ArticlesDetailsPage,
    ArticlesPage,
    MainPage,
    NotFoundPage,
    ProfilePage,
} from '@/pages'
import { AdminPanelPage } from '@/pages/AdminPanelPage'
import { ArticleEditPage } from '@/pages/ArticleEditPage'
import { ForbiddenPage } from '@/pages/ForbiddenPage'
import {
    AppRouteEnum,
    getRouteAbout,
    getRouteAdmin,
    getRouteArticleDetails,
    getRouteArticleEdit,
    getRouteArticleNews,
    getRouteArticles,
    getRouteArticlesForbidden,
    getRouteMain,
    getRouteNotFound,
    getRouteProfile,
} from '@/shared/const/router'
import { AppRouterProps } from '@/shared/types/router'

export const routerConfig: Record<AppRouteEnum, AppRouterProps> = {
    [AppRouteEnum.MAIN]: {
        path: getRouteMain(),
        element: <MainPage />,
        authOnly: false,
    },
    [AppRouteEnum.ABOUT]: {
        path: getRouteAbout(),
        element: <AboutPage />,
        authOnly: false,
    },
    [AppRouteEnum.PROFILE]: {
        path: getRouteProfile(':id'),
        element: <ProfilePage />,
        authOnly: true,
    },
    [AppRouteEnum.ARTICLES]: {
        path: getRouteArticles(),
        element: <ArticlesPage />,
        authOnly: true,
    },
    [AppRouteEnum.ARTICLES_DETAILS]: {
        path: getRouteArticleDetails(':id'),
        element: <ArticlesDetailsPage />,
        authOnly: true,
    },
    [AppRouteEnum.ARTICLE_CREATE]: {
        path: getRouteArticleNews(),
        element: <ArticleEditPage />,
        authOnly: true,
    },
    [AppRouteEnum.ARTICLE_EDIT]: {
        path: getRouteArticleEdit(':id'),
        element: <ArticleEditPage />,
        authOnly: true,
    },
    [AppRouteEnum.ADMIN_PANEL]: {
        path: getRouteAdmin(),
        element: <AdminPanelPage />,
        authOnly: false,
        roles: [UserRoleEnum.ADMIN, UserRoleEnum.MANAGER],
    },
    [AppRouteEnum.FORBIDDEN]: {
        path: getRouteArticlesForbidden(),
        element: <ForbiddenPage />,
    },
    [AppRouteEnum.NOT_FOUND]: {
        path: getRouteNotFound(),
        element: <NotFoundPage />,
        authOnly: false,
    },
}
