import { getUserAuthData, getUserRoles } from '@/entities/User'
import { UserRoleEnum } from '@/entities/User/model/const/const'
import { getRouteMain } from '@/shared/const/router'
import { useMemo } from 'react'
import { useSelector } from 'react-redux'
import { Navigate, useLocation } from 'react-router-dom'

export interface RequireAuthProps {
    children: JSX.Element
    roles?: UserRoleEnum[]
}

export function RequireAuth({ children, roles }: RequireAuthProps) {
    const auth = useSelector(getUserAuthData)
    const location = useLocation()
    const userRoles = useSelector(getUserRoles)

    // eslint-disable-next-line consistent-return
    const hasRequiredRoles = useMemo(() => {
        if (!roles) {
            return true
        }

        return roles.some((requiredRole) => {
            const hasRole = userRoles?.includes(requiredRole)
            return hasRole
        })
    }, [roles, userRoles])

    if (!auth) {
        return <Navigate to={getRouteMain()} state={{ from: location }} replace />
    }

    if (!hasRequiredRoles) {
        return <Navigate to={getRouteMain()} state={{ from: location }} replace />
    }

    return children
}
