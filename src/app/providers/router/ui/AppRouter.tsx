import { AppRouterProps } from '@/shared/types/router'
import { Loader } from '@/widgets/Loader'
import { memo, Suspense, useCallback } from 'react'
import { Route, Routes } from 'react-router-dom'
import { routerConfig } from '../config/routeConfig'
import { RequireAuth } from './RequireAuth'

const AppRouter = () => {
    const renderWithWrapper = useCallback((route: AppRouterProps) => {
        const element = <Suspense fallback={<Loader />}>{route.element}</Suspense>

        return (
            <Route
                key={route.path}
                path={route.path}
                element={
                    route.authOnly ? (
                        <RequireAuth roles={route.roles}>{element}</RequireAuth>
                    ) : (
                        element
                    )
                }
            />
        )
    }, [])

    return <Routes>{Object.values(routerConfig).map(renderWithWrapper)}</Routes>
}

export default memo(AppRouter)
