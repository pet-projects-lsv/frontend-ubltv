import {
    AnyAction,
    CombinedState,
    EnhancedStore,
    Reducer,
    ReducersMapObject
} from '@reduxjs/toolkit';
import { AxiosInstance } from 'axios';
import { IArticleDetailsSchema } from '@/entities/Article';
import { IUserSchema } from '@/entities/User';
import { IAddCommentFormSchema } from '@/features/addCommentForm/model/types/addCommentFormSchema';
import { ILoginSchema } from '@/features/AuthByUsername';
import { IProfileSchema } from '@/features/editableProfileCard/model/types/editableProfileCardSchema';
import { IUISchema } from '@/features/UI';
import { IArticleDetailsPageSchema } from '@/pages/ArticlesDetailsPage/model/types';
import { IArticlesPageSchema } from '@/pages/ArticlesPage';
import { rtkApi } from '@/shared/api/rtkApi';

export interface IStateSchema {
    user: IUserSchema
    ui: IUISchema
    [rtkApi.reducerPath]: ReturnType<typeof rtkApi.reducer>

    // Асинхронные редюсеры
    loginForm?: ILoginSchema
    profile?: IProfileSchema
    articleDetails?: IArticleDetailsSchema
    addCommentForm?: IAddCommentFormSchema
    articlesPage?: IArticlesPageSchema
    articleDetailsPage?: IArticleDetailsPageSchema
}

export type IStateSchemaKey = keyof IStateSchema
export type MountedReducers = OptionalRecord<IStateSchemaKey, boolean>

export interface IReducerManager {
    getReducerMap: () => ReducersMapObject<IStateSchema>
    reduce: (
        state: IStateSchema,
        action: AnyAction
    ) => CombinedState<IStateSchema>
    add: (key: IStateSchemaKey, reducer: Reducer) => void
    remove: (key: IStateSchemaKey) => void
    getMountedReducers: () => MountedReducers
}

export interface IReduxStoreWithManager extends EnhancedStore<IStateSchema> {
    reducerManager: IReducerManager
}

export interface IThunkExtraArg {
    api: AxiosInstance
}

export interface IThunkConfig<T> {
    rejectValue: T
    extra: IThunkExtraArg
    state: IStateSchema
}
