import { StoreProvider } from './ui/StoreProvider';
import { IStateSchema, IThunkConfig } from './config/StateSchema';
import { AppDispatch, createReduxStore } from './config/store';

export type { IReduxStoreWithManager } from './config/StateSchema';

export { StoreProvider, createReduxStore };

export type { AppDispatch, IStateSchema, IThunkConfig };
