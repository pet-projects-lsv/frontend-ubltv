import path from 'path'
import type webpack from 'webpack'
import { buildWebpackConfig } from './config/build/buildWebpackConfig'
import { IBuildMode, type IBuildEnv, type IBuildPaths } from './config/build/types/config'

function getApiUrl(mode: IBuildMode, apiUrl: string) {
    if (apiUrl) {
        return apiUrl
    }

    if (mode === 'production') {
        return '/api'
    }

    return 'http://localhost:8000'
}

export default (env: IBuildEnv) => {
    const paths: IBuildPaths = {
        build: path.resolve(__dirname, 'build'),
        entry: path.resolve(__dirname, 'src', 'index.tsx'),
        html: path.resolve(__dirname, 'public', 'index.html'),
        src: path.resolve(__dirname, 'src'),
        locales: path.resolve(__dirname, 'public', 'locales'),
        buildLocales: path.resolve(__dirname, 'build', 'locales'),
    }

    const mode = env?.mode || 'development';
    const PORT = env?.port || 3000;
    const apiUrl = getApiUrl(mode, env?.apiUrl);

    const analyzer = Boolean(env.analyzer)
    const isDev = mode === 'development';

    const config: webpack.Configuration = buildWebpackConfig({
        mode,
        paths,
        isDev,
        port: PORT,
        analyzer,
        apiUrl,
        project: 'frontend',
    })

    return config
}
