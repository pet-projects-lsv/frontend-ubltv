import path from 'path'

module.exports = {
    globals: {
        __IS_DEV__: true,
        __API__: '',
        __PROJECT__: 'jest',
    },
    testEnvironment: 'jsdom',
    clearMocks: true,
    coveragePathIgnorePatterns: ['/node_modules/'],
    moduleDirectories: ['src', 'node_modules'],
    moduleFileExtensions: ['js', 'mjs', 'cjs', 'jsx', 'ts', 'tsx', 'json', 'node'],
    testMatch: ['**/__tests__/**/*.[jt]s?(x)', '**/?(*.)+(spec|test).[jt]s?(x)'],
    rootDir: '../../',
    setupFilesAfterEnv: ['<rootDir>/config/jest/jestSetup.ts'],
    modulePaths: ['<rootDir>src'],
    // moduleNameMapper: {
    //     '\\.s?css$': 'identity-obj-proxy',
    //     '\\.svg': path.resolve(__dirname, 'jestEmptyComponent.tsx')
    // },
    moduleNameMapper: {
        '\\.s?css$': 'identity-obj-proxy',
        '\\.svg': path.resolve(__dirname, 'jestEmptyComponent.tsx'),
        '^@/(.*)$': '<rootDir>/src/$1',
    },
    // reporters: [
    //     'default',
    //     ['jest-html-reporters', {
    //         publicPath: '<rootDir>/reports/unit',
    //         filename: 'report.html',
    //         openReport: true,
    //         inlineSource: true,
    //     }],
    // ],
}
